const supertest = require("supertest");
const test = require("./test.js");
const request = supertest(test.baseURL);

module.exports = request;
