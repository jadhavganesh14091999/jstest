require("dotenv").config();

const request = require("../config/common.js");
const { faker } = require("@faker-js/faker");
const { expect } = require("chai");
// const { createRandomUser } = require("./helper/userHelper");
const createUser = require("./helper/userHelper.js");
const createRandomUser = createUser.createRandomUser;
const createRandomUserWithFaker = createUser.createRandomUserWithFaker;

const token = process.env.USER_TOKEN;
// console.log(token);
// const token =
//   "ac09330c881c13057d7e09c55358bab3646721cae59016996eaf06175cf63bfc";

describe("User posts", () => {
  let postId;
  let userId;

  before(async () => {
    // userId = await createRandomUser();
    userId = await createRandomUserWithFaker();
  });

  describe("POST", () => {
    it("/posts", async () => {
      const data = {
        user_id: userId,
        title: faker.name.findName(),
        body: faker.lorem.paragraph(),
      };
      const res = await request
        .post("/posts")
        .set("Authorization", `Bearer ${token}`)
        .send(data);

      expect(res.status).to.be.eql(201);
      expect(res.body).to.deep.include(data);
      postId = res.body.id;
    });
  });

  describe("GET", () => {
    it("/posts", async () => {
      const res = await request.get("/posts");
      expect(res.body).to.be.not.empty;
      //   console.log(res.body);
    });

    it("/posts/:id", async () => {
      const res = await request
        .get(`/posts/${postId}`)
        .set("Authorization", `Bearer ${token}`);
      expect(res.status).to.be.eql(200);
      expect(res.body).to.be.not.empty;
    });
  });

  describe("Negative Test", () => {
    it("401 Authentication failed", async () => {
      const data = {
        user_id: userId,
        title: faker.name.findName(),
        body: faker.lorem.paragraph(),
      };
      const res = await request.post("/posts").send(data);
      expect(res.status).to.be.eql(401);
      expect(res.body.message).to.eql("Authentication failed");
    });

    it("422 Data validation failed", async () => {
      const data = {
        user_id: userId,
        title: faker.name.findName(),
      };
      const res = await request
        .post("/posts")
        .set("Authorization", `Bearer ${token}`)
        .send(data);

      expect(res.status).to.be.eql(422);
      expect(res.body[0].field).to.eq("body");
      expect(res.body[0].message).to.eq("can't be blank");

      //   expect(res.body.message).to.eql("Data validation failed");
    });
  });
});
