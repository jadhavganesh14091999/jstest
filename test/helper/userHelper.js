const { faker } = require("@faker-js/faker");
const supertest = require("supertest");
const request = supertest("https://gorest.co.in/public/v2");

const token =
  "ac09330c881c13057d7e09c55358bab3646721cae59016996eaf06175cf63bfc";

const createRandomUserWithFaker = async () => {
  const userData = {
    email: faker.internet.email(),
    name: faker.name.findName(),
    gender: "Male",
    status: "active",
  };
  const res = await request
    .post("/users")
    .set("Authorization", `Bearer ${token}`)
    .send(userData);
  return res.body.id;
};
const createRandomUser = async () => {
  const userData = {
    email: `test${Math.floor(Math.random() * 9999)}@gmail.com`,
    name: "Test",
    gender: "male",
    status: "active",
  };
  const res = await request
    .post("/users")
    .set("Authorization", `Bearer ${token}`)
    .send(userData);

  return res.body.id;
};

module.exports = {
  createRandomUser: createRandomUser,
  createRandomUserWithFaker: createRandomUserWithFaker,
};
