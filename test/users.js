require("dotenv").config();

const request = require("../config/common.js");

const { expect } = require("chai");

const token = process.env.USER_TOKEN;

// const token =
//   "ac09330c881c13057d7e09c55358bab3646721cae59016996eaf06175cf63bfc";

describe("Users", () => {
  let userId;
  describe("POST", () => {
    it("/users", async () => {
      const data = {
        email: `test${Math.floor(Math.random() * 9999)}@gmail.com`,
        name: "Test",
        gender: "male",
        status: "active",
      };
      const res = await request
        .post("/users")
        .set("Authorization", `Bearer ${token}`)
        .send(data);

      userId = res.body.id;

      // expect(res.body.email).to.eq(data.email);
      // expect(res.body.status).to.eq(data.status);
      // expect(res.body.gender).to.eq(data.gender);

      // data.email = "test@gmail.com";
      // data.status = "inactive";

      // Match all the field in data
      expect(res.body).to.deep.include(data);
    });
  });

  describe("GET", () => {
    it("/users", async () => {
      const res = await request.get(`/users?access-token=${token}`);
      expect(res.body).to.be.not.empty;

      // request.get(`/users?access-token=${token}`).end((err, res) => {
      //   expect(res.body).to.be.not.empty;
      //   done();
      // });
    });

    it("/users/:id", async () => {
      const res = await request.get(`/users/${userId}?access-token=${token}`);
      expect(res.body.id).to.be.eql(userId);
    });

    it("/users with query params", async () => {
      const url = `/users?access-token=${token}&page=7&gender=male&status=active`;
      const res = await request.get(url);
      expect(res.body).to.be.not.empty;
      res.body.forEach((user) => {
        expect(user.gender).to.eq("male");
        expect(user.status).to.eq("active");
      });
    });
  });

  // it.only to run specific test
  describe("PUT", () => {
    it("/users/:id", async () => {
      const data = {
        name: `test- ${Math.floor(Math.random() * 9999)}`,
        status: "active",
      };
      const res = await request
        .put(`/users/${userId}`)
        .set("Authorization", `Bearer ${token}`)
        .send(data);
      expect(res.body).to.deep.include(data);
    });
  });

  describe("DELETE", () => {
    it("DELETE /users/:id", async () => {
      const res = await request
        .delete(`/users/${userId}`)
        .set("Authorization", `Bearer ${token}`);
      expect(res.status).to.be.eql(204);
    });
  });
});
